# Python custom Docker image project

This projects hosts Hubstair's custom public Python Docker images. The purpose
of intermediary images is to speed up CI/CD builds and other Docker-based
tasks.

These Docker images come in several flavours that are organised as branches
which are *not* meant to be merged into *master*.

Current images:

- `3.6-sci`: Python 3 with *scientific* packages pre-installed
